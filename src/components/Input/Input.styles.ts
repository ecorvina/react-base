import styled from 'styled-components';
import { InputProps } from './@types/Input.types';

export const InputField = styled.input`
  border: 1px solid ${({ theme, error }: InputProps) => (error ? theme.error : theme.primary)};
  padding: 5px;
`;
