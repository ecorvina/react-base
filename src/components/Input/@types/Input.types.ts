import { ThemePropsType } from '../../../config/@types/theme.types';

export type InputProps = ThemePropsType & {
  error: boolean;
};
