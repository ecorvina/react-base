import React from 'react';
import { ModalBackdrop, ModalCloseButton, ModalContent } from './Modal.styles';
import { ModalProps } from './@types/Modal.types';

export const Modal = ({ children, isOpen, onClose }: ModalProps) => {
  return (
    <>
      {isOpen && (
        <ModalBackdrop>
          <ModalContent>
            <ModalCloseButton onClick={() => onClose()}>X</ModalCloseButton>
            <div>{children}</div>
          </ModalContent>
        </ModalBackdrop>
      )}
    </>
  );
};
