import styled from 'styled-components';
import { ThemePropsType } from '../../config/@types/theme.types';

export const ModalBackdrop = styled.div`
  position: fixed;
  z-index: 800;
  width: 100vw;
  height: 100vh;
  background: rgba(0, 0, 0, 0.7);
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;

export const ModalContent = styled.div`
  border-radius: 8px;
  background-color: white;
  width: 600px;
  height: 500px;
  padding: 8px;
  position: relative;
  cursor: initial;
`;

export const ModalCloseButton = styled.button`
  width: 32px;
  height: 32px;
  background: ${({ theme }: ThemePropsType) => theme.primary};
  border-radius: 100%;
  position: absolute;
  top: -16px;
  right: -16px;
  cursor: pointer;
  border: 0;
  color: white;
`;
