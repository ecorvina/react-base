import React from 'react';
import { Link } from 'react-router-dom';
import { routes } from '../../config/routes';
import { Loading } from '../Loading/Loading';
import { LayoutProps } from './@types/Layout.types';

export const Layout = ({ children }: LayoutProps) => {
  return (
    <div>
      <nav>
        <Link to={routes.home}>Home</Link>
        <Link to={routes.about}>About</Link>
      </nav>
      <Loading />
      <div>{children}</div>
    </div>
  );
};
