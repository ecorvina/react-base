import styled from 'styled-components';

export const LoadingContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  top: 0;
  left: 0;
  background-color: black;
  color: cornflowerblue;
  font-weight: bold;
  font-size: 30px;
  width: 100vw;
  height: 100vh;
  z-index: 1000;
`;
