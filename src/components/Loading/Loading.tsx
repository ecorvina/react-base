import React from 'react';
import { useAppSelector } from '../../store/hooks';
import { LoadingContainer } from './Loading.styles';

export const Loading = () => {
  const { isLoading } = useAppSelector((state) => state.loading);

  return <>{isLoading && <LoadingContainer>Loading</LoadingContainer>}</>;
};
