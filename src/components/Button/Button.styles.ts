import styled from 'styled-components';
import { ThemePropsType } from '../../config/@types/theme.types';

export const Button = styled.button`
  background: ${({ theme }: ThemePropsType) => theme.primary};
  color: white;
  padding: 8px 16px;
  font-weight: bold;
  text-transform: uppercase;
  border: 0;
  border-radius: 8px;
  transition: all ease-in-out 0.2s;
  cursor: pointer;

  &:hover {
    opacity: 0.8;
  }
`;
