import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TodoItem } from '../../pages/Home/hooks/@types/useHome.types';

type TodoState = {
  todos: TodoItem[];
};

const initialState: TodoState = {
  todos: [],
};

const todoSlice = createSlice({
  name: 'todo',
  initialState,
  reducers: {
    setTodos(state, { payload }: PayloadAction<TodoItem[]>) {
      state.todos = payload;
    },
  },
});

export const { setTodos } = todoSlice.actions;
export const todoReducer = todoSlice.reducer;
