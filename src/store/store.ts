import { configureStore } from '@reduxjs/toolkit';
import { loadingReducer } from './loading/loading.slice';
import { todoReducer } from './todos/todos.slice';
import { modalReducer } from './modal/modal.slice';

export const store = configureStore({
  reducer: {
    loading: loadingReducer,
    todo: todoReducer,
    modal: modalReducer,
  },
});
