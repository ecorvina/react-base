import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type ModalState = {
  editTodoModal: {
    isOpen: boolean;
    todoId: number;
  };
};

const initialState: ModalState = {
  editTodoModal: {
    isOpen: false,
    todoId: 0,
  },
};

const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    openEditTodoModal(state) {
      state.editTodoModal.isOpen = true;
    },
    closeEditTodoModal(state) {
      state.editTodoModal.isOpen = false;
    },
    setEditTodoModalTodoId(state, { payload }: PayloadAction<number>) {
      state.editTodoModal.todoId = payload;
    },
  },
});

export const { openEditTodoModal, closeEditTodoModal, setEditTodoModalTodoId } = modalSlice.actions;
export const modalReducer = modalSlice.reducer;
