import { createSlice } from '@reduxjs/toolkit';

type LoadingState = {
  isLoading: boolean;
};

const initialState: LoadingState = {
  isLoading: false,
};

const loadingSlice = createSlice({
  name: 'loading',
  initialState,
  reducers: {
    loadingStart(state) {
      state.isLoading = true;
    },
    loadingFinish(state) {
      state.isLoading = false;
    },
  },
});

export const { loadingStart, loadingFinish } = loadingSlice.actions;
export const loadingReducer = loadingSlice.reducer;
