import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { routes } from './config/routes';
import { About } from './pages/About/About';
import { Home } from './pages/Home/Home';
import { ThemeProvider } from 'styled-components';
import { theme } from './config/theme';
import { Provider } from 'react-redux';
import { store } from './store/store';

export const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <Routes>
            <Route path={routes.about} element={<About />} />
            <Route path={routes.home} element={<Home />} />
          </Routes>
        </ThemeProvider>
      </BrowserRouter>
    </Provider>
  );
};
