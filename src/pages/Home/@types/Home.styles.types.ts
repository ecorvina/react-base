import { ThemePropsType } from '../../../config/@types/theme.types';

export type TodoItemProps = ThemePropsType & {
  completed: boolean;
};
