import React from 'react';
import { TodoItem, TodoItemContainer } from '../../Home.styles';
import { useAppDispatch, useAppSelector } from '../../../../store/hooks';
import { openEditTodoModal, setEditTodoModalTodoId } from '../../../../store/modal/modal.slice';

export const TodoItemList = () => {
  const dispatch = useAppDispatch();
  const { todos } = useAppSelector((state) => state.todo);

  const editTodoItem = (todoId: number) => {
    dispatch(setEditTodoModalTodoId(todoId));
    dispatch(openEditTodoModal());
  };

  return (
    <TodoItemContainer>
      {todos.map(({ id, todo, completed }) => (
        <TodoItem onClick={() => editTodoItem(id)} completed={completed} key={id}>
          {todo}
        </TodoItem>
      ))}
    </TodoItemContainer>
  );
};
