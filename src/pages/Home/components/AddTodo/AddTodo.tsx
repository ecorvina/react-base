import React from 'react';
import { useAddTodo } from './hooks/useAddTodo';
import * as Yup from 'yup';
import { Field, Form, Formik, FormikValues } from 'formik';
import { isButtonDisabled } from '../../../../utils/isButtonDisabled';
import { Button } from '../../../../components/Button/Button.styles';
import { InputField } from '../../../../components/Input/Input.styles';

const AddTodoSchema = Yup.object().shape({
  todo: Yup.string().required().min(6),
});

export const AddTodo = () => {
  const { addTodo } = useAddTodo();

  return (
    <Formik
      initialValues={{
        todo: '',
      }}
      validateOnMount={true}
      validationSchema={AddTodoSchema}
      onSubmit={async ({ todo }: FormikValues) => {
        addTodo(todo);
      }}
    >
      {({ errors, touched, isSubmitting, values }) => (
        <Form>
          <Field as={InputField} name="todo" errors={errors} touched={touched} value={values.todo} />
          <Button type="submit" disabled={isButtonDisabled(touched, errors, isSubmitting)}>
            Add todo
          </Button>
        </Form>
      )}
    </Formik>
  );
};
