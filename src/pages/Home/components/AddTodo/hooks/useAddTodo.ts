import { useAppDispatch, useAppSelector } from '../../../../../store/hooks';
import { TodoItem } from '../../../hooks/@types/useHome.types';
import { setTodos } from '../../../../../store/todos/todos.slice';

export const useAddTodo = () => {
  const { todos } = useAppSelector((state) => state.todo);
  const dispatch = useAppDispatch();

  const addTodo = (todo: string) => {
    const todoToCreate: TodoItem = {
      id: 8123,
      userId: 1211,
      todo,
      completed: false,
    };
    dispatch(setTodos([todoToCreate, ...todos]));
  };

  return {
    addTodo,
  };
};
