import { useAppDispatch, useAppSelector } from '../../../../store/hooks';
import { useCallback, useEffect, useRef } from 'react';
import { getApi } from '../../../../config/api';
import { closeEditTodoModal } from '../../../../store/modal/modal.slice';
import { TodoItem } from '../../hooks/@types/useHome.types';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const EditTodoSchema = Yup.object().shape({
  todo: Yup.string().required().min(6),
});

export const useEditTodoModal = () => {
  const { isOpen, todoId } = useAppSelector((state) => state.modal.editTodoModal);
  const dispatch = useAppDispatch();
  const formikRef = useRef({ todo: '' });
  const formik = useFormik({
    initialValues: {
      todo: '',
    },
    validationSchema: EditTodoSchema,
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  const onClose = () => dispatch(closeEditTodoModal());

  const fetchTodo = useCallback(async () => {
    const {
      data: { todo },
    } = await getApi().get<TodoItem>(`/todos/${todoId}`);
    formik.setFieldValue('todo', todo);
  }, [formik, todoId]);

  useEffect(() => {
    if (isOpen) {
      fetchTodo();
    }
  }, [fetchTodo, isOpen]);

  return {
    onClose,
    isOpen,
    formikRef,
    formik,
  };
};
