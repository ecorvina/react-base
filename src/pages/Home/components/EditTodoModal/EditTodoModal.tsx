import { Modal } from '../../../../components/Modal/Modal';
import { Field, FormikProvider } from 'formik';
import { InputField } from '../../../../components/Input/Input.styles';
import { Button } from '../../../../components/Button/Button.styles';
import { isButtonDisabled } from '../../../../utils/isButtonDisabled';
import React from 'react';
import { useEditTodoModal } from './useEditTodoModal';

export const EditTodoModal = () => {
  const { isOpen, onClose, formik } = useEditTodoModal();

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <FormikProvider value={formik}>
        <form>
          <Field
            as={InputField}
            name="todo"
            errors={formik.errors}
            touched={formik.touched}
            value={formik.values.todo}
          />
          <Button type="submit" disabled={isButtonDisabled(formik.touched, formik.errors, formik.isSubmitting)}>
            Save
          </Button>
        </form>
      </FormikProvider>
    </Modal>
  );
};
