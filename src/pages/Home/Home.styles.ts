import styled from 'styled-components';
import { TodoItemProps } from './@types/Home.styles.types';
import { ThemePropsType } from '../../config/@types/theme.types';

export const CenteredText = styled.div`
  text-align: center;
  margin-bottom: 16px;
`;

export const TodoItem = styled(CenteredText)`
  color: ${({ theme }: ThemePropsType) => theme.primary};
  border: 1px solid ${({ theme }: ThemePropsType) => theme.primary};
  border-radius: 8px;
  padding: 8px;
  width: 360px;
  height: 80px;
  text-decoration: ${({ completed }: TodoItemProps) => (completed ? 'line-through' : 'initial')};

  @media print {
    color: ${({ theme }: ThemePropsType) => theme.print.primary};
    border-color: ${({ theme }: ThemePropsType) => theme.print.primary};
  }
`;

export const TodoItemContainer = styled.div`
  display: flex;
  margin: 0 auto;
  gap: 8px;
  width: 768px;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
`;
