import React from 'react';
import { useHome } from './hooks/useHome';
import { CenteredText } from './Home.styles';
import { Button } from '../../components/Button/Button.styles';
import { ButtonContainer } from '../../components/Containers/ButtonContainer.styles';
import { Layout } from '../../components/Layout/Layout';
import { TodoItemList } from './components/TodoItemList/TodoItemList';
import { AddTodo } from './components/AddTodo/AddTodo';
import { EditTodoModal } from './components/EditTodoModal/EditTodoModal';
export const Home = () => {
  const { fetchTodos } = useHome();

  return (
    <Layout>
      <CenteredText>
        <h2>Todos</h2>
      </CenteredText>
      <CenteredText>
        <AddTodo />
      </CenteredText>
      <ButtonContainer>
        <Button onClick={fetchTodos}>Refresh content</Button>
      </ButtonContainer>
      <TodoItemList />
      <EditTodoModal />
    </Layout>
  );
};
