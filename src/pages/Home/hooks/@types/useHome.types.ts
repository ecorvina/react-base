export type IdWithUserId = {
  id: number;
  userId: number;
};

export type TodoItem = IdWithUserId & {
  todo: string;
  completed: boolean;
};

export type TodoApiResponse = {
  limit: number;
  skip: number;
  todos: TodoItem[];
  total: number;
};

// export interface IdWithUserId {
//   id: number;
//   userId: number;
// }
//
// export interface TodoItem extends IdWithUserId {
//   todo: string;
//   completed: boolean;
// }
//
// export interface TodoApiResponse {
//   limit: number;
//   skip: number;
//   todos: TodoItem[];
//   total: number;
// }
