import { useCallback, useEffect } from 'react';
import { getApi } from '../../../config/api';
import { TodoApiResponse } from './@types/useHome.types';
import { useAppDispatch } from '../../../store/hooks';
import { loadingFinish, loadingStart } from '../../../store/loading/loading.slice';
import { setTodos } from '../../../store/todos/todos.slice';

export const useHome = () => {
  const dispatch = useAppDispatch();

  const fetchTodos = useCallback(async () => {
    dispatch(loadingStart());
    try {
      const {
        data: { todos: todoResponseItemList },
      } = await getApi().get<TodoApiResponse>('/todos');

      dispatch(setTodos(todoResponseItemList));
    } catch (err) {
      console.error(err);
    } finally {
      dispatch(loadingFinish());
    }
  }, [dispatch]);

  useEffect(() => {
    fetchTodos();
  }, [fetchTodos]);

  return {
    fetchTodos,
  };
};
