import axios from 'axios';

export const API_URL = process.env.REACT_APP_API_URL;

const apiInstance = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

export const getApi = () => {
  apiInstance.interceptors.request.use((config) => {
    // console.log(`this url have been called ${config.url}`);
    return config;
  });

  apiInstance.interceptors.response.use((response) => {
    // console.log(response);
    return response;
  });

  return apiInstance;
};

const noHeaderApiInstance = axios.create({
  baseURL: API_URL,
});

export const getNoHeaderApi = () => {
  return noHeaderApiInstance;
};
