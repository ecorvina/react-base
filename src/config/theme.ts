import { ThemeType } from './@types/theme.types';

export const theme: ThemeType = {
  primary: 'cornflowerblue',
  accent: 'gray',
  error: 'red',
  print: {
    primary: 'black',
    accent: 'white',
  },
};
