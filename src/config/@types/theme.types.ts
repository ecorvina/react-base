export type ThemeType = {
  accent: string;
  primary: string;
  error: string;
  print: {
    accent: string;
    primary: string;
  };
};

export type ThemePropsType = {
  theme: ThemeType;
};
