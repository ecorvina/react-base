import { FormikErrors, FormikTouched, FormikValues } from 'formik';

export const isButtonDisabled = (
  touched: FormikTouched<FormikValues>,
  errors: FormikErrors<FormikValues>,
  isSubmitting: boolean
): boolean => Object.keys(touched).some((fieldName) => Object.keys(errors).includes(fieldName)) || isSubmitting;
